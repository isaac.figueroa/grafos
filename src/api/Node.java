package api;

/**
 *
 * @author Isaac Figueroa
 */
public class Node {
    public int id;
    public int degree;
    public double x, y;
    public boolean explored;
    
    public Node(int id){
        this.id = id;
        this.degree = 0;
        this.explored = false;
    }
    
    public Node(int id, double x, double y){
        this.id = id;
        this.degree = 0;
        this.x = x;
        this.y = y;
        this.explored = false;
    }
}
