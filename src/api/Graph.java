package api;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author Isaac Figueroa
 */
public class Graph {
    public HashMap<Integer, Node> N;
    public HashMap<String, Edge> E;
    public boolean directed;
    public boolean autocycle;
    public int n;
    
    private String method;
    private String method_value;
    
    private void initGraph(String method, String method_value, int n, boolean directed, boolean autocycle){
        this.N = new HashMap();
        this.E = new HashMap();
        
        this.directed = directed;
        this.autocycle = autocycle;
        this.method = method;
        this.method_value = method_value;
        this.n = n;
    }
    
    public Graph(String method, String method_value, int n, boolean directed, boolean autocycle, boolean set_coordinates){
        initGraph(method, method_value, n, directed, autocycle);
        n++;
        if(set_coordinates){
            for(int i = 1; i < n; i++)  this.N.put(i, new Node(i, Math.random(), Math.random()));
        }
        else{
            for(int i = 1; i < n; i++)  this.N.put(i, new Node(i));
        }
    }
    
    public Graph(String method, int root_node, Graph graph){
        initGraph(method + "_" + graph.method, String.valueOf(root_node), graph.n, false, false);
        graph.N.entrySet().forEach((node) -> {
           this.N.put(node.getKey(), node.getValue()); 
        });
    }
    
    public boolean toGraphViz(){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String fileName = this.method + "_" + this.n + "_" + this.method_value + "_" + String.valueOf(timestamp.getTime());
            try (FileWriter writer = new FileWriter(fileName + ".gv")) {
                writer.write((this.directed ? "digraph " : "graph ") + fileName + " {\r\n");
                SortedSet<String> keys = new TreeSet<>(this.E.keySet());
                for(String key : keys)  writer.write(key + ";\r\n");
                writer.write("}");
            }
        } catch (IOException e) {
            return false;
        }
        
        return true;
    }
}
