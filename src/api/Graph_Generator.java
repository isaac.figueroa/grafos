package api;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Isaac Figueroa
 */
public class Graph_Generator {
    
    private Graph graph = null;
    private Graph tree = null;
    
    private boolean stablishEdge(int node1, int node2, boolean check_nodesExistance){
        //if both nodes are the same and it is not allowed autocycles return false
        if(node1 == node2 && !this.graph.autocycle)    return false;
             
        //check if both nodes already exist, if not create them
        boolean node1_exists = true, node2_exists = true;
        if(check_nodesExistance){
            node1_exists = this.graph.N.containsKey(node1);
            node2_exists = this.graph.N.containsKey(node2);
            if(!node1_exists) this.graph.N.put(node1, new Node(node1));
            if(!node2_exists) this.graph.N.put(node2, new Node(node2));
        }   
             
        //only if before this function calling both nodes already existed, it is sensitive to check the edge list
        if(node1_exists && node2_exists){
        //if an edge between both nodes already exist, return false
        if(existEdge(String.valueOf(node1), String.valueOf(node2)))    return false;
        }
             
        //stablish new edge id: node1-->node2 or node1--node2, depending of the case
        String edge = node1 + (this.graph.directed ? "->" : "--") + node2;
             
        //add new edge
        this.graph.E.put(edge, new Edge(edge, this.graph.N.get(node1), this.graph.N.get(node2)));
             
        return true;
    }
    
    private boolean existEdge(String node1, String node2){
        if(this.graph.directed){
            if(this.graph.E.containsKey(node1 + "-->" + node2) || 
               this.graph.E.containsKey(node2 + "-->" + node1)){
                return true;
            }
        }
        else{
            if(this.graph.E.containsKey(node1 + "--" + node2) || 
               this.graph.E.containsKey(node2 + "--" + node1)){
                return true;
            }
        }
        
        return false;
    }
    
    public Graph ErdosRenyi(int n, int m, boolean directed, boolean autocycle){
        graph = new Graph("ErdosRenyi", String.valueOf(m), n, directed, autocycle, false);  //create set of n nodes
        int i = 0;
        int lim = n + 1;
        //search for random pair nodes
        while(i < m){
            //check if the edge is going to be stablished
            if(stablishEdge(ThreadLocalRandom.current().nextInt(1, lim),
                              ThreadLocalRandom.current().nextInt(1, lim), false)){
                i++;    //if true, the index increase
                //System.out.println(i);
            }
        }
        
        return graph;
    }
    
    private String replace_dot(float value, String to_replace){
        String original_value = String.valueOf(value);
        String replaced_value = original_value.replace("0.", to_replace);
        if(replaced_value.equals(original_value))    replaced_value = replaced_value.replace(".", to_replace);
        
        return replaced_value;
    }
    
    public Graph Gilbert(int n, float p, boolean directed, boolean autocycle){
        graph = new Graph("Gilbert", replace_dot(p, "p"), n, directed, autocycle, false);  //create set of n nodes
        
        //check all the posible pair of nodes
        for(int i = 1; i <= n; i++){
            for(int j = 1; j <= n; j++){
                if(Math.random() <= p)  //if a random value is less or equal to p
                    stablishEdge(j , i, false);    //stablish the edge
            }
            //System.out.println(i);
        }
        
        return graph;
    }
    
    public Graph BarabasiAlbert(int n, int d, boolean directed, boolean autocycle){
        graph = new Graph("BarabasiAlbert", String.valueOf(d), 0, directed, autocycle, false);  //init Graph with any node
        graph.n = n;    //only for the name generated at the end
        n++;
        for(int u = 1; u < n; u++){ //foreach desired node
            Node node_u = new Node(u);  //create node u
            this.graph.N.put(u, node_u);
            for(int v = 1; v <= u; v++){
                Node node_v = this.graph.N.get(v);    //get node v
                if(node_v.degree  < d){ //if the degree of node v is less than the maximum posible
                    //if the probability satisfy, it is checked if the edge between u and v will be stablished
                    if(Math.random() <= (1 - ((double)node_v.degree / (double)d))){
                        if(stablishEdge(u , v, false)){    //if the edge here is stablished
                            //update the degree of node u and v
                            node_u.degree++;    this.graph.N.put(u, node_u);
                            if(u != v || autocycle){
                                node_v.degree++;    this.graph.N.put(v, node_v);
                            }
                            //System.out.println(u + "--" + v);
                        }
                    }
                }
            }
        }
        
        return graph;
    }
    
    public Graph Geographic(int n, float r, boolean directed, boolean autocycle){
        //create set of n nodes with x & y position over [0,1) each
        graph = new Graph("Geographic", replace_dot(r, "r"), n, directed, autocycle, true);
        
        //check all the posible pair of nodes
        for(int i = 1; i <= n; i++){
            for(int j = 1; j <= n; j++){
                //if the Euclidian distance between both nodes is equal or less than r
                //stablish the edge if it doesn´t exist
                if(getDistance(j, i) <= r)  stablishEdge(j , i, false);
            }
            //System.out.println(i);
        }
        
        return graph;
    }
    
    private double getDistance(int u, int v){
        //get both nodes
        Node node_u = this.graph.N.get(u);  Node node_v = this.graph.N.get(v);
        
        //returns Euclidian distance
        return Math.sqrt(Math.pow(node_u.x - node_v.x, 2) + Math.pow(node_u.y - node_v.y, 2));
    }
    
    public Graph BFS(Graph graph, int s){
        
        //initialize Tree with all the nodes of graph and indicate s as the root node
        this.tree = new Graph("BFS", s, graph);
        
        //initialize array flag
        boolean[] discovered = new boolean[this.tree.n];
        for(int j = 0; j < this.tree.n; j++) discovered[j] = false;
        discovered[s - 1] = true;   //set s flag true
        
        //Layer container
        ArrayList<ArrayList<Node>> L = new ArrayList<>();   //initialize layers container
        ArrayList<Node> L0 = new ArrayList<>();    //initialize layer 0
        L0.add(graph.N.get(s));    //add s to layer 0
        L.add(L0); //add layer 0 to layers container
        int i = 0;  //initialize layer counter
        
        //while the current layer is not empty
        while(!L.get(i).isEmpty()){
            ArrayList<Node> Li_1 = new ArrayList<>();    //initialize layer i + 1
            ArrayList<Node> Li = L.get(i);  //get current layer i
            Li.forEach((u) -> {
                //check all the incident edges to u
                graph.E.entrySet().forEach((edge_entry) -> {
                    //get the testing edge
                    Edge edge = edge_entry.getValue();
                    String[] nodes = graph.directed ? edge.id.split("->") : edge.id.split("--");
                    
                    //if the current testing edge contains the u Node
                    if (u.id == Integer.parseInt(nodes[0]) || u.id == Integer.parseInt(nodes[1])) {
                        Node v = u.id == Integer.parseInt(nodes[0]) ? edge.node2 : edge.node1;  //get Node v
                        if (!discovered[v.id - 1]) {    //if Node v hasn´t been discovered yet
                            discovered[v.id - 1] = true;    //change the flag status
                            this.tree.E.put(edge.id, edge);  //set the edge to the tree
                            Li_1.add(v);    //add the Node v to the layer i + 1
                        }
                    }
                });
            });
            L.add(Li_1);    //add layer i + 1 to the layers container
            i++;    //increment the layer counter
        }
        
        return this.tree;
    }
    
    public Graph DFS_I(Graph graph, int s){
        
        //initialize Tree with all the nodes of graph and indicate s as the root node
        this.tree = new Graph("DFSI", s, graph);
        
        Node[] parent = new Node[graph.n];
        
        //initialize array flag
        boolean[] explored = new boolean[this.tree.n];
        for(int j = 0; j < this.tree.n; j++) explored[j] = false;
        
        //initialize stack container
        Stack<Node> S = new Stack<>();
        S.push(graph.N.get(s)); //push the s node to the stack
        
        //while the stack is not empty
        while(!S.isEmpty()){
            Node u = S.pop();  //get last Node added to the Stack and remove it
            
            //if such node hasn´t been explored
            if(!explored[u.id - 1]){
                explored[u.id - 1] = true;  //change the flag
                if(u.id != s){  //if it is different to s
                    //stablish new edge id: u-->parent[u] or u--parent[u], depending of the case
                    String edge_ID = u.id + (graph.directed ? "->" : "--") + parent[u.id - 1].id;
             
                    //add new edge to the DFS tree
                    this.tree.E.put(edge_ID, new Edge(edge_ID, u, parent[u.id - 1]));
                }
                
                //check all the incident edges
                graph.E.entrySet().forEach((edge_entry) -> {
                    //get the testing edge
                    Edge edge = edge_entry.getValue();
                    String[] nodes = graph.directed ? edge.id.split("->") : edge.id.split("--");
                    
                    //if the current test edge contains the u Node
                    if (u.id == Integer.parseInt(nodes[0]) || u.id == Integer.parseInt(nodes[1])) {
                        //add Node v to the stack
                        Node v = u.id == Integer.parseInt(nodes[0]) ? edge.node2 : edge.node1;
                        S.push(v);
                        parent[v.id - 1] = u;   //update the parent of the Node v
                    }
                });
            }
        }
        
        return tree;
    }
    
    public Graph DFS_R(Graph graph, int s){
        //initialize Tree with all the nodes of graph and indicate s as the root node
        this.tree = new Graph("DFSR", s, graph);
        this.graph = graph;
        DFS_R(graph.N.get(s));
        
        return tree;
    }
    
    private void DFS_R(Node u){
        u.explored = true;  //Mark u as explored
        this.tree.N.put(u.id, u);   //add u to the tree
                
        //check all the incident edges
        this.graph.E.entrySet().forEach((edge_entry) -> {
            //get the testing edge
            Edge edge = edge_entry.getValue();
            String[] nodes = this.graph.directed ? edge.id.split("->") : edge.id.split("--");
            int node1 = Integer.parseInt(nodes[0]); int node2 = Integer.parseInt(nodes[1]);
                    
            //if the current test edge contains the u Node
            if (u.id == node1 || u.id == node2) {
                //get the node v from the tree according to the nodes of the current edge from the graph
                Node v = u.id == node1 ? this.tree.N.get(node2) : this.tree.N.get(node1);
                if(!v.explored){ //if v has not been explored
                    this.tree.E.put(edge.id, new Edge(edge.id, u, v));  //add new edge to the DFS tree
                    DFS_R(v);    //recursively invoke this function
                }
            }
        });
        
    }
}
