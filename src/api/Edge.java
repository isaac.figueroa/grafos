package api;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lenovo
 */
public class Edge {
    public String id;
    public Node node1;
    public Node node2;
    
    public Edge(String id, Node node1, Node node2){
        this.id = id;
        this.node1 = node1;
        this.node2 = node2;
    }
}
